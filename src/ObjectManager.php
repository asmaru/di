<?php

declare(strict_types=1);

namespace asmaru\di;

use Exception;
use ReflectionClass;
use ReflectionException;
use ReflectionFunction;
use function sprintf;

/**
 * Class ObjectManager
 *
 * @package asmaru\di
 */
class ObjectManager {

	/**
	 * @var array
	 */
	private array $factories = [];

	/**
	 * @var array
	 */
	private array $instances = [];

	/**
	 * ObjectManager constructor.
	 */
	public function __construct() {
		$this->service(static::class, $this);
	}

	/**
	 * Register a class factory
	 *
	 * @param string $class The fully qualified class name
	 * @param mixed $factory A callable factory function or a instance object
	 */
	public function register(string $class, mixed $factory = null): void {
		$this->factories[$class] = ['factory' => $factory, 'service' => false, 'user' => true];
	}

	/**
	 * Register a service factory. A service is a singleton instance.
	 *
	 * @param string $class The fully qualified class name
	 * @param mixed $factory A callable factory function or a instance object
	 */
	public function service(string $class, mixed $factory = null): void {
		$this->factories[$class] = ['factory' => $factory, 'service' => true, 'user' => true];
	}

	/**
	 * Create a new instance of the given class
	 *
	 * @template T of object
	 * @param class-string<T> $class
	 * @param array $params
	 *
	 * @return T the created instance
	 * @noinspection PhpDocSignatureInspection
	 */
	public function make(string $class, array $params = []): object {
		// check if this class should be instantiated only once and return the instance if one exists
		$isService = isset($this->factories[$class]) && $this->factories[$class]['service'];
		if ($isService) {
			$instance = $this->instances[$class] ?? null;
			if (!empty($instance)) return $instance;
		}

		// get factory function for class or create a default factory if no custom function was provided
		if (!isset($this->factories[$class]) || empty($this->factories[$class]['factory'])) {
			$this->factories[$class] = ['factory' => $this->getDefaultFactory($class), 'service' => $isService, 'user' => false];
		}

		$factory = $this->factories[$class]['factory'];
		$user = $this->factories[$class]['user'];

		// create the new instance
		$instance = is_callable($factory) ? ($user ? $factory() : $factory($params)) : $factory;

		// store the service instance
		if ($isService) {
			$this->instances[$class] = $instance;
		}

		return $instance;
	}

	/**
	 * Call a closure with required parameters
	 *
	 * @throws ReflectionException
	 */
	public function call(callable $function): mixed {
		$reflection = new ReflectionFunction($function);
		$args = [];
		foreach ($reflection->getParameters() as $param) {
			$args[$param->getName()] = $this->make($param->getType()->getName());
		}
		return $reflection->invokeArgs($args);
	}

	/**
	 * Create a default factory when no custom factory was provided
	 */
	private function getDefaultFactory(string $class): callable {
		return function (array $params = []) use ($class) {
			$reflectionClass = new ReflectionClass($class);
			$constructor = $reflectionClass->getConstructor();
			$args = [];
			if ($constructor !== null) {
				foreach ($constructor->getParameters() as $param) {
					if ($param === null) throw new Exception(sprintf('Parameter is null in constructor for class "%s"', $class));
					if (isset($params[$param->getName()])) {
						$args[$param->getName()] = $params[$param->getName()];
						continue;
					}
					if ($param->getType() === null) throw new Exception(sprintf('Parameter %s in class %s is no valid class', $param->getName(), $class));
					$args[$param->getName()] = $this->make($param->getType()->getName());
				}
			}
			return $reflectionClass->newInstanceArgs($args);
		};
	}
}