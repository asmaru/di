# Readme

![Codacy grade](https://img.shields.io/codacy/grade/f9bcb955fd4c4457a0dfff546c121844?style=for-the-badge)
![Bitbucket Pipelines](https://img.shields.io/bitbucket/pipelines/asmaru/di/master?style=for-the-badge)
![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg?style=for-the-badge)

## Usage

```php
$om = new ObjectManager();

$om->register(ClassA::class, new ClassA());

$om->register(ClassA::class, function () {
	return new ClassA();
});

$om->service(ClassA::class, function () {
	return new ClassA();
});

$fn = function (ClassA $a) {
};

$instance = $om->make(ClassA::class);

$om->call($fn);
```