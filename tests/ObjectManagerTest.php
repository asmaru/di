<?php

namespace asmaru\di;

use PHPUnit\Framework\TestCase;
use ReflectionException;

/**
 * Class ObjectManagerTest
 *
 * @covers \asmaru\di\ObjectManager
 */
class ObjectManagerTest extends TestCase {

	public function testMakeWithoutDependencies() {
		$di = new ObjectManager();
		/** @var ClassA $a */
		$a = $di->make(ClassA::class);
		$this->assertInstanceOf(ClassA::class, $a);
	}

	public function testMakeWithDependencies() {
		$di = new ObjectManager();
		/** @var ClassB $b */
		$b = $di->make(ClassB::class);
		$this->assertInstanceOf(ClassB::class, $b);
		$this->assertInstanceOf(ClassA::class, $b->a);
	}

	public function testMakeWithDependenciesMultipleParams() {
		$di = new ObjectManager();
		$di->register(ClassC::class, fn() => new ClassC(null));
		/** @var ClassD $d */
		$d = $di->make(ClassD::class);
		$this->assertInstanceOf(ClassD::class, $d);
		$this->assertInstanceOf(ClassA::class, $d->a);
		$this->assertInstanceOf(ClassB::class, $d->b);
		$this->assertInstanceOf(ClassA::class, $d->b->a);
		$this->assertInstanceOf(ClassC::class, $d->c);
	}

	public function testService() {
		$di = new ObjectManager();
		$di->service(ClassA::class, new ClassA());
		/** @var ClassB $c */
		$b1 = $di->make(ClassB::class);
		/** @var ClassB $c */
		$b2 = $di->make(ClassB::class);
		$this->assertNotEquals(spl_object_hash($b1), spl_object_hash($b2));
		$this->assertEquals(spl_object_hash($b1->a), spl_object_hash($b2->a));
	}

	public function testRegisterFactory() {
		$expected = uniqid(true);
		$di = new ObjectManager();
		$di->register(ClassC::class, fn() => new ClassC($expected));
		/** @var ClassC $c */
		$c = $di->make(ClassC::class);
		$this->assertInstanceOf(ClassC::class, $c);
		$this->assertEquals($expected, $c->stringParam);
	}

	/**
	 * @throws ReflectionException
	 */
	public function testCall() {
		$di = new ObjectManager();
		$this->assertInstanceOf(ClassB::class, $di->call(fn(ClassB $b) => $b));
	}
}
