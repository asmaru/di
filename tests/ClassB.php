<?php

namespace asmaru\di;

class ClassB {

	public function __construct(public ClassA $a) {
	}
}