<?php

namespace asmaru\di;

class ClassD {

	public function __construct(public ClassA $a, public ClassB $b, public ClassC $c) {
	}
}